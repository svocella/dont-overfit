\documentclass[11pt,twocolumn,a4paper]{article}

\usepackage{graphicx}

\begin{document}

\newcommand{\xv}{{\bf x}}
\newcommand{\wv}{{\bf w}}
\newcommand{\sigmav}{\mbox{\boldmath$\sigma$}}

\title{How to prevent overfitting?}
\author{
Simon Vocella\\
\small \texttt{s.vocella@campus.unimib.it}\\
Corso di Data \& Text Mining\\
Dipartimento di Informatica, Sistemistica e Comunicazione (DISCo)\\
Universit\`a degli Studi di Milano – Bicocca
}

\maketitle

\begin{abstract}
The aim of this study is to evaluate methods and techniques to prevent overfitting. In this paper It's presented and illustrated different methods: SVM, SVC, penalizedSVM, Fscore and Random Forest. Each method is written in R. The dataset used is the kaggle dataset practice Don't overfit! \cite{dontoverfit}: a dataset with 200 features and 20000 record, 250 of the training set and 19750 of the test set.
\end{abstract}

\section{Introduction}
In case of overfitting, the model generally takes the form of making an overly complex model to explain idiosyncrasies in the data under study. In reality, the data being studied often has some degree of error or random noise within it. Thus attempting to make the model conform too closely to slightly inaccurate data can infect the model with substantial errors and reduce its predictive power.  
In order to avoid overfitting, it is necessary to use additional techniques (e.g. cross-validation, regularization, early stopping, pruning, Bayesian priors on parameters or model comparison), that can indicate when further training is not resulting in better generalization.\\
In the next chapters we describe the algorithms and models used to prevent overfitting, to compare these methods we use the balanced error rate (BER) or area under of curve (AUC) where AUC = 1-BER.

\section{SVM}
Support vector machines as introduced by Vapnik~\cite{Vapnik95,Vapnik98}. 
Given a set of $n$ input vectors
$\xv_i$ and outputs $y_i\in\{-1,+1\}$, one tries to find a weight
vector $\wv$ and offset $b$ defining a hyperplane that maximally
separates the examples. This can be formalized as the minimization
problem
\begin{equation}
\min_{\wv,b,e} \frac{1}{2} \wv\cdot\wv^T + C\sum_{i=1}^n e_i \
\end{equation}
\begin{equation}
\mbox{subject to} \quad y_i(\wv\cdot\xv_i+b) \ge 1 \
\end{equation}
In our specific case It will be used the RBF kernel. 

\section{SVC}
The full SVM algorithm requires choosing the value of C, which controls the tradeoff between fitting the data and obtaining a large margin. In practice, to choose C, we must rely on holdout methods and cross-validation methods.\\
Holdout methods are the best way to choose a classifier and cross-validation methods are the best way to set a regularization parameters (in our case C and sigma).\\
The procedure SVC (Support Vector Classification) is the following:
\begin{enumerate}
\item Consider a grid space with $\log_2 C$ between [-5, 15] and $\log_2 sigma$ between [-15, 3]
\item For each hyperparameter pair (C, sigma) in the search space, conduct 5-fold cross validation on the training set.
\item Choose the parameter (C, sigma) that leads to the lowest cross-validation BER.
\item Use the best parameter to create a model as the predictor.
\end{enumerate}
The procedure is developed in R with a wrapper on LIBSVM \cite{CC01a}.

\section{PenalizedSVM}
One way to prevent overfitting is reducing the features through techniques of feature selection. In this method the features will be ranked via variance-based sensitivity analysis. Variance-based sensitivity analysis is a form of global sensitivity analysis. \cite{gsa} Working within a probabilistic framework, it decomposes the variance of the output of the model or system into fractions which can be attributed to inputs or sets of inputs. For example, given a model with two inputs and one output, one might find that 70\% of the output variance is caused by the variance in the first input, 20\% by the variance in the second, and 10\% due to interactions between the two. These percentages are directly interpreted as measures of sensitivity. Variance-based measures of sensitivity are attractive because they measure sensitivity across the whole input space (i.e. it is a global method), they can deal with nonlinear responses, and they can measure the effect of interactions in non-additive systems. \cite{saltelliannoni}\\
The resulting procedure penalizedSVM:
\begin{enumerate}
\item Divide randomly the dataset in train and validation set (holdout)
\item Train the model SVC with train set
\item Do prediction with model and validation set
\item Get the variable importance and remove the feature less important through variance-based sensitivity analysis
\item We repeat this until the subset of feature have only the class feature
\item In the end we choose the best feature subset that have the highest AUC
\end{enumerate}
\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{preprocessing.eps}
\end{figure}
In our case the method choose 142 features and obtain an AUC for the test set of 0.8575186.

\section{F-score Feature Selection}
In this section the method presented is the F-score Feature Selection \cite{Chen06}, it's not accurate as the previous but it's fast. The resulting procedure is:
\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{fscore.eps}
\end{figure}
\begin{enumerate}
\item Calculate F-score of every feature
\item Pick some possible low thresholds to cut low F-scores 
\item For each threshold, do the following
\begin{itemize}
\item Drop features with F-score below this threshold
\item Randomly split the training data into Xtrain and Xvalid
\item Let Xtrain be the new training data. Use the SVC model to obtain a predictor; use the predictor to predict Xvalid
\item Repeat the steps above 5 times, and then calculate the average validation error
\end{itemize}
\item Choose the threshold with the lowest average validation error (so the highest average AUC)
\end{enumerate}
In our case we choose as low thresholds $L \in [0.0001, 0.003]$ and the best threshold is 0.003 with an AUC of 0.840896933971922 for the test set.

\section{Random Forest}
The Random Forest is appropriate for high dimensional data modeling because it can handle missing values and can handle continuous, categorical and binary data. The bootstrapping and ensemble scheme makes random forest strong enough to overcome the problems of over fitting and hence there is no need to prune the trees. Besides high prediction accuracy, Random Forest is efficient, interpretable and non-parametric for various types of datasets \cite{Statistics01randomforests}. These are the promises that led us to choose Random Forest. After a few tries the Random Forest model suffers of overfitting more than SVM so in this case the method "rf+GINI" is developed to overcome the overfitting: 
\begin{enumerate}
\item Initialize the RF working data set to include all training instances. 
\item Calculate the index of Gini of each feature and obtain the rank of features.
\item Use RF as a predictor and conduct 5-fold CV on the working set.
\item Update the working set by removing $V$ features (in our case 5) which are less important and go to the previous step. Stop if the number of features is small.
\item Among various feature subsets chosen above, select one with the lowest BER.
\end{enumerate}
\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{randomForest.eps}
\end{figure}
The features is ranked by the index of Gini because it's defined as a measure of "node impurity" in tree-based classification.\\
A low Gini means that a particular predictor variable plays a greater role in partitioning the data into the defined classes.\\
In our case the method choose 138 featuress with a AUC for the test set of 0.848461995932111, better than the AUC in case of the Random Forest alone: 0.6629637.

\section{Conclusion}
In the graphic each method is compared by the AUC of the test and train set (blue and red in figure). The better method is penalizedSVM, followed by fscore and rf+GINI, so the promises described above about overfitting is false. In the case of SVC the method choose as C=3 and sigma=0.0078125.
\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{auc.eps}
\end{figure}
This study demonstrates various methods and tecniques to prevent or limit overfitting to have a robust algorithm. These results are just a starting point and could be enhaced. In particular, we can found C and sigma with linear programming tools like CPLEX or glpk, we can enhace the other used models, we can use other new models as LibLINEAR that maybe can overcome overfitting in other better ways.

\bibliographystyle{plain}
\bibliography{bibliography}

\end{document}
