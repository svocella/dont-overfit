library("randomForest")
source("svc.R")

rfcv <- function (trainx, trainy, cv.fold = 5, scale = "log", step = 0.5, 
    mtry = function(p) max(1, floor(sqrt(p))), recursive = FALSE, 
    ...) 
{
    classRF <- is.factor(trainy)
    n <- nrow(trainx)
    p <- ncol(trainx)
    if (scale == "log") {
        k <- floor(log(p, base = 1/step))
        n.var <- round(p * step^(0:(k - 1)))
        same <- diff(n.var) == 0
        if (any(same)) 
            n.var <- n.var[-which(same)]
        if (!1 %in% n.var) 
            n.var <- c(n.var, 1)
    }
    else {
        n.var <- seq(from = p, to = 1, by = step)
    }
    k <- length(n.var)
    cv.pred <- vector(k, mode = "list")
    imp.var <- list()
    for (i in 1:k) {
		cv.pred[[i]] <- trainy
		imp.var[[i]] <- list()
		for (j in 1:cv.fold) {
				imp.var[[i]][[j]] <- list()
		}
    }
    if (classRF) {
        f <- trainy
    }
    else {
        f <- factor(rep(1:5, length = length(trainy))[order(order(trainy))])
    }
    nlvl <- table(f)
    idx <- numeric(n)
    for (i in 1:length(nlvl)) {
        idx[which(f == levels(f)[i])] <- sample(rep(1:cv.fold, 
            length = nlvl[i]))
    }
    for (i in 1:cv.fold) {
        all.rf <- randomForest(trainx[idx != i, , drop = FALSE], 
            trainy[idx != i], trainx[idx == i, , drop = FALSE], 
            trainy[idx == i], mtry = mtry(p), importance = TRUE, 
            ...)
        
        cv.pred[[1]][idx == i] <- all.rf$test$predicted
        impvar <- (1:p)[order(all.rf$importance[, 1], decreasing = TRUE)]
		imp.var[[1]][[i]] <- impvar
        for (j in 2:k) {
            imp.idx <- impvar[1:n.var[j]]
            sub.rf <- randomForest(trainx[idx != i, imp.idx, 
                drop = FALSE], trainy[idx != i], trainx[idx == 
                i, imp.idx, drop = FALSE], trainy[idx == i], 
                mtry = mtry(n.var[j]), importance = recursive, 
                ...)
            cv.pred[[j]][idx == i] <- sub.rf$test$predicted
		    imp.var[[j]][[i]] <- names(trainx[, imp.idx])

            if (recursive) {
                impvar <- (1:length(imp.idx))[order(sub.rf$importance[, 
                  1], decreasing = TRUE)]
            }
            NULL
        }
        NULL
    }
    if (classRF) {
        error.cv <- sapply(cv.pred, function(x) mean(trainy != 
            x))
    }
    else {
        error.cv <- sapply(cv.pred, function(x) mean((trainy - 
            x)^2))
    }
    names(error.cv) <- names(cv.pred) <- n.var
    list(n.var = n.var, error.cv = error.cv, predicted = cv.pred, impvar = imp.var)
}

result <- rfcv(trainset[,-1], trainset$class, scale="normal", step = -5)
#result <- rfcv(trainset[,-1], trainset$class)
setEPS()
postscript("randomForest.eps")
with(result, plot(n.var, error.cv, log="x", type="o", lwd=2, main = "Random Forest", xlab = "Variables", ylab = "BER" ))
dev.off()

minErrorCV <- which(result$error.cv == min(result$error.cv))[[1]]

for(i in 1:5) {
model = svc(class~., trainset[, c(result$impvar[[minErrorCV]][[i]], "class")])
predTest = predict(model$model, testset[, result$impvar[[minErrorCV]][[i]]])
auc <- mmetric(testset$class, predTest, "AUC")
print(paste("Test AUC: ", auc))
}

#modelRF = randomForest(class~., data=trainset, importance = TRUE)

# Grafico di varImpPlot con type 2
#varImpPlot(modelRF, type=2, n.var=200)


#importance
#importance(modelRF, type=2)

#varUsed(modelRF)
